//
//  MXMainController.m
//  Demo
//
//  Created by Maxime Lecomte on 03/28/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "MXMainController.h"
#import "MXGridControllerSearch.h"
#import "PSTCollectionViewCell.h"
#import "PSTCollectionView.h"
#import "MXCollectionViewWaterfallCell.h"


#define CELL_IDENTIFIER @"WaterfallCell"


@interface MXMainController ()

@property(nonatomic, strong) UIImageView *menuBar;
@property(nonatomic, strong) MXGridController *gridController;
@property(nonatomic, strong) NSArray *images;
@property(nonatomic) int numberOfItems;
@end

@implementation MXMainController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.gridController = [[MXGridControllerSearch alloc] initWitCellHeight:165 andNumberOfRow:4];
        self.gridController.delegate = self;

        self.images = @[@"http://download.fotolia.com/Content/CompImage/FotoliaComp_44851408_x8dVFmKH4fJ1xV4L91tvgmiraYL9N9nB",
                @"http://t1.ftcdn.net/jpg/00/49/79/68/400_F_49796871_884gZPnchqRvYEeXKPBDHLN5dzxJwzhv.jpg",
                @"http://t1.ftcdn.net/jpg/00/43/64/34/110_F_43643440_YfOrNGHDpXXXVTmAZwB9avKVjGQ8VBQt.jpg",
                @"http://download.fotolia.com/Content/CompImage/FotoliaComp_45101071_cmdu74jE8odERoXJkKdFQnOwmvwInypF",
                @"http://download.fotolia.com/Content/CompImage/FotoliaComp_40462294_uAJdPfOuuWxtS1v24qQUF8QLfFbgI742"
        ];
        self.numberOfItems = 10;
        [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(addItems:) userInfo:nil repeats:YES];
    }
    return self;
}

- (void)addItems:(id)addItems
{
    //TODO:
    NSLog(@"additems");
    [self.gridController refreshContent];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addChildViewController:self.gridController];
    self.gridController.view.frame = self.view.bounds;
    [self.view addSubview:self.gridController.view];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];


    self.menuBar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bottom_bar.png"]];
    self.menuBar.frame = CGRectMake(0, 698, 1024, 50);
    [self.view addSubview:self.menuBar];
}

- (PSTCollectionViewCell *)collectionView:(PSTCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MXCollectionViewWaterfallCell * cell = (MXCollectionViewWaterfallCell *)
    [collectionView dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];

    cell.displayString = [NSString stringWithFormat:@"%d", indexPath.row];
    NSInteger randomIndex = arc4random() % 5;
    NSString *imageURl = self.images[randomIndex];
    [cell setImageViewWithURlString:imageURl];

    return cell;
}

- (NSString *)gridControllerCellIdentifier:(MXGridController *)gridController
{
    return CELL_IDENTIFIER;
}

- (CGFloat)gridController:(MXGridController *)gridController widthForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (NSInteger)gridControllerNumberOfItemInSection:(NSInteger)section
{
    return self.numberOfItems += 10;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
