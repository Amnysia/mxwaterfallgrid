//
//  MXGridController.h
//  Demo
//
//  Created by Nelson on 12/11/27.
//  Copyright (c) 2012年 Nelson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXWaterFallLayout.h"

@class MXGridController;

@protocol MXGridControllerDelegate <NSObject>

@required
- (PSTCollectionViewCell *)collectionView:(PSTCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
- (CGFloat) gridController:(MXGridController *)gridController widthForItemAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *)gridControllerCellIdentifier:(MXGridController *)gridController;
- (NSInteger)gridControllerNumberOfItemInSection:(NSInteger)section;

@end

@interface MXGridController : UIViewController <PSTCollectionViewDataSource, MXCollectionViewDelegateWaterfallLayout>

@property (nonatomic, strong) PSTCollectionView *collectionView;
@property(weak, nonatomic) id <MXGridControllerDelegate> delegate;

- (id)initWithLayout:(MXWaterFallLayout *)layout cellHeight:(CGFloat)cellheight andNumberOfRow:(NSInteger)numberOfRow;


- (void)refreshContent;

@end
