//
//  MXCollectionViewWaterfallCell.m
//  Demo
//
//  Created by Nelson on 12/11/27.
//  Copyright (c) 2012年 Nelson. All rights reserved.
//

#import "MXCollectionViewWaterfallCell.h"
#import "UIImageView+AFNetworking.h"

@interface MXCollectionViewWaterfallCell ()
@property(nonatomic, strong) UILabel *displayLabel;
@property(nonatomic, strong) UIView *blackBackgroundView;
@end

@implementation MXCollectionViewWaterfallCell

#pragma mark - Accessors
- (UILabel *)displayLabel
{
    if (!_displayLabel) {
        _displayLabel = [[UILabel alloc] initWithFrame:self.contentView.bounds];
        _displayLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _displayLabel.backgroundColor = [UIColor lightGrayColor];
        _displayLabel.textColor = [UIColor whiteColor];
        _displayLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _displayLabel;
}

- (void)setDisplayString:(NSString *)displayString
{
//    self.blackBackgroundView.alpha = 1;
    if (![_displayString isEqualToString:displayString]) {
        _displayString = [displayString copy];
        self.displayLabel.text = _displayString;
    }

    self.blackBackgroundView.frame = self.contentView.bounds;
    self.blackBackgroundView.alpha = 1.;



    [UIView animateWithDuration:.5 delay:.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        self.blackBackgroundView.alpha = 0.;
    } completion:^(BOOL finished) {

    }];

//    NSLog(@"Cell number : %@", self.displayString);

}

#pragma mark - Life Cycle
- (void)dealloc
{
    [_displayLabel removeFromSuperview];
    _displayLabel = nil;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self.contentView addSubview:self.displayLabel];
                self.blackBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
                self.blackBackgroundView.backgroundColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.];
                self.blackBackgroundView.alpha = 1.;
                [self.contentView addSubview:self.blackBackgroundView];

                self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
                self.imageView.contentMode = UIViewContentModeScaleAspectFill;
                self.imageView.clipsToBounds = YES;
                [self.contentView insertSubview:self.imageView belowSubview:self.blackBackgroundView];
    }
    return self;
}

- (void)setImageViewWithURlString:(NSString *)imageUrlString
{
    self.imageView.frame = self.contentView.bounds;
    NSURL *imageURL = [NSURL URLWithString:imageUrlString relativeToURL:nil];
    [self.imageView setImageWithURL:imageURL];

}
@end
