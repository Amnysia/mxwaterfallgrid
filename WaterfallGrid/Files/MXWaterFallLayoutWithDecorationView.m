//
// Created by maxlecomte on 28/03/13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <CoreGraphics/CoreGraphics.h>
#import "MXWaterFallLayoutWithDecorationView.h"
#import "MXSearchHeader.h"
#import "PSTCollectionView.h"


@implementation MXWaterFallLayoutWithDecorationView

- (void)prepareLayout
{
    [super prepareLayout];
    [self.collectionView.collectionViewLayout registerClass:[MXSearchHeader class] forDecorationViewOfKind:@"HEADER"];
}

-(NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *attributes = [[super layoutAttributesForElementsInRect:rect] mutableCopy];
    PSTCollectionViewLayoutAttributes *layoutAttributes = [self layoutAttributesForDecorationViewOfKind:@"HEADER" atIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    [attributes insertObject:layoutAttributes atIndex:0];

    return attributes;
}

- (PSTCollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind atIndexPath:(NSIndexPath *)indexPath
{
    PSTCollectionViewLayoutAttributes *layoutAttributes = [PSTCollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:decorationViewKind withIndexPath:indexPath];
    layoutAttributes.frame = CGRectMake(0.0, 0.0, self.startOffset.x, self.collectionViewContentSize.height);
    layoutAttributes.zIndex = 100;
    return layoutAttributes;
}
@end