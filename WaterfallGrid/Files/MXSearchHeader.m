//
// Created by maxlecomte on 26/03/13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "MXSearchHeader.h"
#import "MXMainController.h"


@interface MXSearchHeader ()
@property(nonatomic, strong) UIImageView *imagevIew;
@end

@implementation MXSearchHeader


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImage *backgroundImage = [UIImage imageNamed:@"sidebar_big.png"];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        imageView.image = backgroundImage;
        [self addSubview:imageView];
    }
    return self;
}


@end