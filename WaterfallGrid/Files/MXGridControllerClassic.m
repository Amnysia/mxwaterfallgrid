//
//  MXGridControllerClassic.m
//  Demo
//
//  Created by Maxime Lecomte on 03/28/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "MXGridControllerClassic.h"
#import "MXGridController.h"

@implementation MXGridControllerClassic


- (id)initWithCellHeight:(NSInteger)cellHeight andNumberOfRow:(NSInteger)numberOfRow
{
    MXWaterFallLayout *layout = [[MXWaterFallLayout alloc] initWithStartOffset:CGPointMake(0, 0)];
    self = [super initWithLayout:layout cellHeight:cellHeight andNumberOfRow:numberOfRow];
    if (self) {

    }
    return self;
}
@end
