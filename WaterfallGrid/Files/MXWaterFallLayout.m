//
//  MXWaterFallLayout.m
//
//  Created by Nelson on 12/11/19.
//  Copyright (c) 2012 Nelson Tai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXWaterFallLayout.h"
#import "PSTCollectionView.h"
#import "MXSearchHeader.h"

@interface MXWaterFallLayout ()
@property (nonatomic, assign) NSInteger itemCount;
@property (nonatomic, assign) CGFloat interitemSpacing;
@property (nonatomic, strong) NSMutableArray *rowWidths; // height for each column
@property (nonatomic, strong) NSMutableArray *itemAttributes; // attributes for each item
@property(nonatomic, strong) NSMutableDictionary *myLayoutInfo;
@end

@implementation MXWaterFallLayout

#pragma mark - Accessors
- (void)setRowCount:(NSUInteger)rowCount
{
    if (_rowCount != rowCount) {
        _rowCount = rowCount;
        [self invalidateLayout];
    }
}

- (void)setItemHeight:(CGFloat)itemHeight
{
    if (_itemHeight != itemHeight) {
        _itemHeight = itemHeight;
        [self invalidateLayout];
    }
}

- (void)setSectionInset:(UIEdgeInsets)sectionInset
{
    if (!UIEdgeInsetsEqualToEdgeInsets(_sectionInset, sectionInset)) {
        _sectionInset = sectionInset;
        [self invalidateLayout];
    }
}

#pragma mark - Init
- (void)commonInit
{
    _rowCount = 3;
    _itemHeight = 140.0f;
    _sectionInset = UIEdgeInsetsZero;
}

- (id)initWithStartOffset:(CGPoint)startOffset
{
    self = [super init];
    if (self) {
        self.startOffset = startOffset;
        [self commonInit];
    }
    return self;
}

#pragma mark - Life cycle
- (void)dealloc
{
    [_rowWidths removeAllObjects];
    _rowWidths = nil;

    [_itemAttributes removeAllObjects];
    _itemAttributes = nil;
}



#pragma mark - Methods to Override
- (void)prepareLayout
{
    [super prepareLayout];
    NSLog(@"Prepare layout");

    _itemCount = [[self collectionView] numberOfItemsInSection:0];

    NSAssert(_rowCount > 1, @"columnCount for MXWaterFallLayout should be greater than 1.");
    CGFloat height = self.collectionView.frame.size.height - _sectionInset.top - _sectionInset.bottom;
    _interitemSpacing = floorf((height - _rowCount * _itemHeight) / (_rowCount - 1));

    _itemAttributes = [NSMutableArray arrayWithCapacity:_itemCount];
    _rowWidths = [NSMutableArray arrayWithCapacity:_rowCount];
    for (NSInteger idx = 0; idx < _rowCount; idx++) {
        [_rowWidths addObject:@(_sectionInset.left + self.startOffset.x)];
    }



    // Item will be put into shortest column.
    for (NSInteger idx = 0; idx < _itemCount; idx++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:idx inSection:0];
        CGFloat itemWidth = [self.delegate collectionView:self.collectionView layout:self widthForItemAtIndexPath:indexPath];
        NSUInteger rowIndex = [self shortestRowIndex];
        CGFloat yOffset = _sectionInset.top + (_itemHeight + _interitemSpacing) * rowIndex;
        CGFloat xOffset = [(_rowWidths[rowIndex]) floatValue];

        PSTCollectionViewLayoutAttributes *attributes = [PSTCollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        attributes.frame = CGRectMake(xOffset, yOffset, itemWidth, self.itemHeight);
        [_itemAttributes addObject:attributes];
        _rowWidths[rowIndex] = @(xOffset + itemWidth + _interitemSpacing);
    }
}

- (CGSize)collectionViewContentSize
{
    if (self.itemCount == 0) {
        return CGSizeZero;
    }

    CGSize contentSize = self.collectionView.frame.size;
    NSUInteger rowIndex = [self longestRowIndex];
    CGFloat width = [self.rowWidths[rowIndex] floatValue];
    contentSize.width = width - self.interitemSpacing + self.sectionInset.right;
    return contentSize;
}

- (PSTCollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)path
{
    return (self.itemAttributes)[path.item];
}



- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *attributes = [[self.itemAttributes filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(PSTCollectionViewLayoutAttributes *evaluatedObject, NSDictionary *bindings) {
        return CGRectIntersectsRect(rect, [evaluatedObject frame]);
    }]]mutableCopy];

    return attributes;
}



- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return NO;
}





#pragma mark - Private Methods
// Find out shortest column.
- (NSUInteger)shortestRowIndex
{
    __block NSUInteger index = 0;
    __block CGFloat shortestWidth = MAXFLOAT;

    [self.rowWidths enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
    {
        CGFloat width = [obj floatValue];
        if (width < shortestWidth) {
            shortestWidth = width;
            index = idx;
        }
    }];

    return index;
}

// Find out longest column.
- (NSUInteger)longestRowIndex
{
    __block NSUInteger index = 0;
    __block CGFloat longestWidth = 0;

    [self.rowWidths enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
    {
        CGFloat width = [obj floatValue];
        if (width > longestWidth) {
            longestWidth = width;
            index = idx;
        }
    }];

    return index;
}

@end
