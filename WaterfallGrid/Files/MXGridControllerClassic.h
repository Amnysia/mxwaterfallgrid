//
//  MXGridControllerClassic.h
//  Demo
//
//  Created by Maxime Lecomte on 03/28/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//



#import "MXGridController.h"

@interface MXGridControllerClassic : MXGridController

- (id)initWithCellHeight:(NSInteger)cellHeight andNumberOfRow:(NSInteger)numberOfRow;
@end
