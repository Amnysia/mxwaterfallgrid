//
//  MXGridController.m
//  Demo
//
//  Created by Nelson on 12/11/27.
//  Copyright (c) 2012年 Nelson. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "MXGridController.h"
#import "MXCollectionViewWaterfallCell.h"
#import "PSTCollectionView.h"
#import "MXSearchHeader.h"

#define CELL_HEIGHT 165


@interface MXGridController ()
@property(nonatomic, strong) NSMutableArray *cellWidths;

@end

@implementation MXGridController
{
    MXWaterFallLayout *_layout;
}


-(id)initWithLayout:(MXWaterFallLayout *)layout cellHeight:(CGFloat)cellheight andNumberOfRow:(NSInteger)numberOfRow
{
    self = [super init];
    if (self) {
        _layout = layout;
        _layout.delegate = self;
        layout.itemHeight = cellheight;
        layout.rowCount = numberOfRow;
        layout.sectionInset = UIEdgeInsetsMake(9, 9, 9, 9);
    }
    return self;
}

#pragma mark - Accessors
- (PSTCollectionView *)collectionView
{
    if (!_collectionView) {
        CGRect collectionViewFrame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height -50);
        _collectionView = [[PSTCollectionView alloc] initWithFrame:collectionViewFrame collectionViewLayout:_layout];
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;

        _collectionView.backgroundColor = [UIColor blackColor];
        [_collectionView registerClass:[MXCollectionViewWaterfallCell class] forCellWithReuseIdentifier:[self.delegate gridControllerCellIdentifier:self]];
    }
    return _collectionView;
}

#pragma mark - Life Cycle
- (void)dealloc
{
    [_collectionView removeFromSuperview];
    _collectionView = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.collectionView];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [_collectionView setContentOffset:_layout.startOffset animated:NO];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(PSTCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.delegate gridControllerNumberOfItemInSection:section];
}

- (NSInteger)numberOfSectionsInCollectionView:(PSTCollectionView *)collectionView
{
    return 1;
}

- (PSTCollectionViewCell *)collectionView:(PSTCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.delegate collectionView:collectionView cellForItemAtIndexPath:indexPath];
}

#pragma mark - UICollectionViewWaterfallLayoutDelegate
- (CGFloat)collectionView:(PSTCollectionView *)collectionView layout:(MXWaterFallLayout *)collectionViewLayout widthForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.delegate gridController:self widthForItemAtIndexPath:indexPath];
}


- (void)refreshContent
{
    [_layout invalidateLayout];
}
@end
