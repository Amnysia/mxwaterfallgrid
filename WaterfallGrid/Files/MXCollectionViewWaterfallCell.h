//
//  MXCollectionViewWaterfallCell.h
//  Demo
//
//  Created by Nelson on 12/11/27.
//  Copyright (c) 2012年 Nelson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTCollectionViewCell.h"

@interface MXCollectionViewWaterfallCell : PSTCollectionViewCell
@property (nonatomic, copy) NSString *displayString;

@property(nonatomic, strong) UIImageView *imageView;

- (void)setImageViewWithURlString:(NSString *)imageUrlString;
@end
