//
//  MXGridControllerSearch.h
//  Demo
//
//  Created by Maxime Lecomte on 03/28/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//



#import "MXGridController.h"

@interface MXGridControllerSearch : MXGridController

- (id)initWitCellHeight:(NSInteger)cellHeight andNumberOfRow:(NSInteger)numberOfRow;
@end
