//
//  MXGridControllerSearch.m
//  Demo
//
//  Created by Maxime Lecomte on 03/28/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "MXGridControllerSearch.h"
#import "MXWaterFallLayoutWithDecorationView.h"
#import "PSTCollectionView.h"


#define kSEARCH_BAR_WIDTH 300


@interface MXGridControllerSearch ()

@property(nonatomic, strong) UIImageView *smallbar;
@property(nonatomic) CGRect smallbarShownFrame;
@property(nonatomic) CGRect smallbarHiddenFrame;
@property(nonatomic, strong) UIImageView *bigbar;
@property(nonatomic) CGRect bigbarHidenFrame;
@property(nonatomic) CGRect bigbarShownFrame;

@end

@implementation MXGridControllerSearch

- (id)initWitCellHeight:(NSInteger)cellHeight andNumberOfRow:(NSInteger)numberOfRow
{
    CGPoint startOffset = CGPointMake(kSEARCH_BAR_WIDTH, 0);
    MXWaterFallLayoutWithDecorationView *layout = [[MXWaterFallLayoutWithDecorationView alloc] initWithStartOffset:startOffset];

    self = [super initWithLayout:layout cellHeight:cellHeight andNumberOfRow:numberOfRow];
    if (self) {

    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.smallbar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sidebar_small.png"]];

    self.smallbarShownFrame = CGRectMake(-5, 5, 51, 698);
    self.smallbarHiddenFrame = CGRectMake(-60, 0, self.smallbar.frame.size.width, self.smallbar.frame.size.height);
    self.smallbar.frame = self.smallbarHiddenFrame;

    self.smallbar.userInteractionEnabled = YES;
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(smalBarTouched:)];
    [self.smallbar addGestureRecognizer:gestureRecognizer];
    [self.view addSubview:self.smallbar];


    self.bigbar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sidebar_big.png"]];
    self.bigbarHidenFrame = CGRectMake(-self.bigbar.frame.size.width, 5, kSEARCH_BAR_WIDTH, 698);
    self.bigbarShownFrame = CGRectMake(-5, 5, self.bigbar.frame.size.width, self.bigbar.frame.size.height);
    self.bigbar.frame = self.bigbarHidenFrame;
    [self.view addSubview:self.bigbar];



}

- (void)hideBothBar
{
    [UIView animateWithDuration:0.3 animations:^
    {
        self.smallbar.alpha = 0.;
        self.bigbar.frame = self.bigbarHidenFrame;
    }];
}

- (void)showBigBar
{
    [UIView animateWithDuration:.3 animations:^
    {
        self.smallbar.frame = self.smallbarHiddenFrame;
    }                completion:^(BOOL finished)
    {
        [UIView animateWithDuration:.5 animations:^
        {
            self.bigbar.frame = self.bigbarShownFrame;
        }];
    }];
}


- (void)showSmallBar
{
    self.smallbar.alpha = 0.;
    self.smallbar.frame = self.smallbarShownFrame;
    if (self.collectionView.contentOffset.x > kSEARCH_BAR_WIDTH) {
        [UIView animateWithDuration:0.5 animations:^
        {
            self.smallbar.alpha = 1.;
        }];
    }
}

- (void)smalBarTouched:(UITapGestureRecognizer *)smalBarTouched
{
    [self showBigBar];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (decelerate == NO) {
        [self showSmallBar];
    }
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self showSmallBar];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self hideBothBar];
}
@end
