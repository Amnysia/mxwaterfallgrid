//
//  MXWaterFallLayout.h
//
//  Created by Nelson on 12/11/19.
//  Copyright (c) 2012 Nelson Tai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTCollectionViewCommon.h"
#import "PSTCollectionViewLayout.h"

@class MXWaterFallLayout;
@protocol MXCollectionViewDelegateWaterfallLayout <PSTCollectionViewDelegate>
- (CGFloat)collectionView:(PSTCollectionView *)collectionView layout:(MXWaterFallLayout *)collectionViewLayout widthForItemAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface MXWaterFallLayout : PSTCollectionViewLayout
@property (nonatomic, weak) id<MXCollectionViewDelegateWaterfallLayout> delegate;
@property (nonatomic, assign) NSUInteger rowCount; // How many columns
@property (nonatomic, assign) CGFloat itemHeight; // Height for every column
@property (nonatomic, assign) UIEdgeInsets sectionInset; // The margins used to lay out content in a section
@property(nonatomic) CGPoint startOffset;

- (id)initWithStartOffset:(CGPoint)startOffset;

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect;
@end
