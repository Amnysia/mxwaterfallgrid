//
//  MXMainController.h
//  Demo
//
//  Created by Maxime Lecomte on 03/28/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//



#import "MXGridController.h"

@class MXGridController;

@interface MXMainController : UIViewController <MXGridControllerDelegate>

@end
